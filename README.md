# Dockerized Nginx VoD module

Based on the [Nginx VoD module](https://github.com/kaltura/nginx-vod-module)

Locally running at:  
http://video-streaming.dkr

## Demo:

### HSL
**Local**: http://video-streaming.dkr/hls/sample-video,.mp4,.urlset/master.m3u8  

### Dash
Does **not** work in most browsers, use QuickTime or VLC to play.  
http://video-streaming.dkr/dash/sample-video,.mp4,.urlset/manifest.mpd

### Generated thumbnail
Change the number according to a video frame to get a different result.  
**Local**: http://video-streaming.dkr/thumb/sample-video.mp4/thumb-51000.jpg  

## Setup:

1. `docker-compose up --build`
2. Copy video(s) into `videos` folder
3. Done.

### URL-formation

**basename : streamname**  
dash : manifest.mpd  
hls master playlist : master.m3u8  
hls media playlist : index.m3u8

**Stream:**  
`[domain] / [dash/hls] / [video name (excluding extension)] , [video extenstion (including ".")] ,.urlset / [streamname]`

**Thumbnail:**  
`[domain] /thumb/ [filename (including extention)] /thumb- [frame] .jpg`
